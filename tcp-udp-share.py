#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.cli import CLI

import time
from threading import Timer

# Global constants
h1_udp_file = "h1-udp.txt"
h2_tcp_file = "h2-tcp.txt"
h3_tcp_file = "h3-tcp.txt"
h3_udp_file = "h3-udp.txt"
h1_time_file = "h1-time.txt"
h2_time_file = "h2-time.txt"
test_duration_sec = 300 # how long to run iperf tests?
unthrottle_duration_sec = 150 # how long afterward to unthrottle TCP?
h3_tcp_stats_file = "tcp-drops-overlimits.txt"
h3_udp_stats_file = "udp-drops-overlimits.txt"
h3_agg_stats_file = "agg-drops-overlimits.txt"
expt_start_time = 0
start_time_file = "expt-start-time.txt"

class SetupTopo(Topo):
    """ Simple topology to setup 2 sources and bottleneck links to
    destination."""

    def __init__(self, **opts):
        Topo.__init__(self, **opts)
        s1 = self.addSwitch('s1')
        s2 = self.addSwitch('s2')
        h1 = self.addHost('h1', cpu=0.10)
        h2 = self.addHost('h2', cpu=0.10)
        h3 = self.addHost('h3', cpu=0.20)
        
        # self.addLink(h1, s1, bw=3, delay='5ms', loss=1,
        #              use_htb=True, max_queue_size=100)
        # self.addLink(h2, s1, bw=3, delay='5ms', loss=1,
        #              use_htb=True, max_queue_size=100)
        # self.addLink(s1, s2, bw=5, delay='5ms', loss=1,
        #              use_htb=True, max_queue_size=100)
        # self.addLink(s2, h3, bw=10, delay='5ms', loss=1,
        #              use_htb=True, max_queue_size=100)

        self.addLink(h1, s1)
        self.addLink(h2, s1)
        self.addLink(s1, s2)
        self.addLink(s2, h3)

def getHosts(net):
    """ Get host Node objects corresponding to host names """
    h1 = net.getNodeByName('h1')
    h2 = net.getNodeByName('h2')
    h3 = net.getNodeByName('h3')
    return [h1, h2, h3]

def getSwitches(net):
    """ Get switch Node objects corresponding to switch names """
    s1 = net.getNodeByName('s1')
    s2 = net.getNodeByName('s2')
    return [s1, s2]
    
def setupTc(net, hosts_list, switches_list):
    """ Set up rate limits and weighted queueing for the network """
    h1, h2, h3 = hosts_list
    s1, s2 = switches_list

    # h1-s1 link capacity
    h1.cmd("tc qdisc add dev h1-eth0 root handle 1: htb default 1")
    h1.cmd("tc class add dev h1-eth0 parent 1: "
           " classid 1:1 htb rate 1.0Mbit")

    # h2-s1 link capacity
    h2.cmd("tc qdisc add dev h2-eth0 root handle 1: htb default 1")
    h2.cmd("tc class add dev h2-eth0 parent 1: "
           " classid 1:1 htb rate 0.40Mbit")

    # s1-s2 link capacity
    s1.cmd("tc qdisc add dev s1-eth3 root handle 1: htb default 10")
    s1.cmd("tc class add dev s1-eth3 parent 1: classid 1:1 htb rate"
           " 0.50Mbit")
    
    # s1-s2 weighted round robin queues
    s1.cmd("tc class add dev s1-eth3 parent 1:1 classid 1:10 htb rate"
           " 0.40Mbit ceil 0.50Mbit")
    s1.cmd("tc class add dev s1-eth3 parent 1:1 classid 1:11 htb rate"
           " 0.10Mbit ceil 0.50Mbit")

    # s1-s2 queue classification based on protocol
    s1.cmd("tc filter add dev s1-eth3 protocol ip parent 1:0 prio 1"
           " u32 match ip protocol 6 0xff flowid 1:10")

    s1.cmd("tc filter add dev s1-eth3 protocol ip parent 1:0 prio 1"
           " u32  match ip protocol 17 0xff flowid 1:11")
 
    # s1-s2 shallow queue buffers -- 100 packets
    s1.cmd("tc qdisc add dev s1-eth3 parent 1:10 handle 20: pfifo"
           " limit 100")
    s1.cmd("tc qdisc add dev s1-eth3 parent 1:11 handle 30: pfifo"
           " limit 100")

def unthrottleFlow(hosts_list):
    """ Unthrottle H2 such that s1--s2 now becomes the single unique
    bottleneck link."""
    h1, h2, h3 = hosts_list

    # unthrottle TCP flow at h2
    h2.cmd("tc class change dev h2-eth0 classid 1:1 htb rate 1.0Mbit")

def startExpt():
    """ Record starting time of the experiment in the variable
    expt_start_time. To be used every time we want to know the time
    elapsed in the experiment. """
    global expt_start_time
    expt_start_time = time.time()
    f = open(start_time_file, "w")
    f.write(str(expt_start_time))
    f.close()

def getExptTime():
    """ Return time since the experiment started """
    return time.time() - expt_start_time
    
def runIperfTest(net, hosts_list, switches_list):
    """ Run iperf testing between hosts in the network and record
    throughput results. """
    h1, h2, h3 = hosts_list
    s1, s2 = switches_list

    # open files to note experiment timestamps
    f1_time = open(h1_time_file, "w")
    f2_time = open(h2_time_file, "w")

    print "Starting iperf servers on h3"
    h3.cmd("iperf -u -s -p 5002 -i 5 > " + h3_udp_file + " &")
    h3.cmd("iperf -s -p 5003 -i 5 > " + h3_tcp_file + " &")
    time.sleep(2) # this seems to be required to allow successful
                  # TCP connection establishment

    print "Starting iperf client transmits"
    startExpt()

    h1.cmd("iperf -t " + str(test_duration_sec) + " -c 10.0.0.3 -u -p"
           " 5002 -i 5 " " -b 100000 " " > "
           + h1_udp_file + " &")
    f1_time.write(str(getExptTime()))

    h2.cmd("iperf -t " + str(test_duration_sec) + " -c 10.0.0.3 -i 5"
           " -p 5003 > " + h2_tcp_file + " &")
    f2_time.write(str(getExptTime()))
    
    f1_time.close()
    f2_time.close()
    
    # also setup unthrottling in a bit
    t = Timer(unthrottle_duration_sec, unthrottleFlow, [hosts_list])
    t.start()
    
def getTcStatCommand(protocol):
    """ Get tc stat command to send to host based on protocol
    (tcp/udp) or stat (drops/overlimits). """

    cmd = "tc -s -d class show dev s1-eth3 classid "

    if protocol == "tcp":
        cmd += "1:10"
    elif protocol == "udp":
        cmd += "1:11"
    elif protocol == "all":
        cmd = "tc -s -d qdisc show dev s1-eth3 "

    cmd += " | grep dropped"
    
    if protocol == "all":
        cmd += " | head -1"

    return cmd
    
def sendCommandWithTime(host, cmd):
    """ Return results from sending a command to a host """
    print "*** running command", cmd, "on host", host.name
    host.sendCmd(cmd)
    result = host.waitOutput()
    return str(getExptTime()) + " " + result.strip() + "\n"

def getTcStats(net, hosts_list, switches_list):
    """ Collect tc stats for drops and overlimits """
    h1, h2, h3 = hosts_list
    s1, s2 = switches_list

    # parts of the statistics extraction command to be stitched
    # together later
    # output files
    tcp_stats = open(h3_tcp_stats_file, "w")
    udp_stats = open(h3_udp_stats_file, "w")
    agg_stats = open(h3_agg_stats_file, "w")

    end_time = time.time() + test_duration_sec
    while time.time() < end_time:
        # Record line containing sent, drops and overlimits
        # TCP stats
        cmd = getTcStatCommand("tcp")
        tcp_stats.write(sendCommandWithTime(s1, cmd))

        # UDP stats
        cmd = getTcStatCommand("udp")
        udp_stats.write(sendCommandWithTime(s1, cmd))

        # aggregate stats
        cmd = getTcStatCommand("all")
        agg_stats.write(sendCommandWithTime(s1, cmd))

        time.sleep(4.5)

    tcp_stats.close()
    udp_stats.close()
    agg_stats.close()
        
def sharingTest():
    """ Main """
    topo = SetupTopo()
    # net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
    net = Mininet(topo=topo, host=CPULimitedHost)
    net.start()

    print "Dumping host connections"
    dumpNodeConnections(net.hosts)

    print "Testing network connectivity"
    net.pingAll()
    
    hosts_list = getHosts(net)
    switches_list = getSwitches(net)

    print "Setting up traffic control"
    setupTc(net, hosts_list, switches_list)

    # test workload in runIperfTest(...)
    print "Starting iperf tests"
    runIperfTest(net, hosts_list, switches_list)
    
    print "Collecting tc stats... this may take a while (",
    print test_duration_sec, "seconds )"
    getTcStats(net, hosts_list, switches_list)

    CLI(net)
    
    net.stop()
    
if __name__ == '__main__':
    # Tell mininet to print useful information
    setLogLevel('info')
    sharingTest()

