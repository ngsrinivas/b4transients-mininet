#!/usr/bin/python
"""
Test work conservation in tc at ~ 100Mbit/s bandwidths.
"""

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.cli import CLI

import time
from threading import Timer

# Global constants
test_duration_sec = 5 # how long to run iperf tests?
unthrottle_duration_sec = 2.5 # how long afterward to unthrottle TCP?
tcp_stats_file = "tcp-drops-overlimits.txt"
udp_stats_file = "udp-drops-overlimits.txt"
agg_stats_file = "agg-drops-overlimits.txt"
expt_start_time = 0
start_time_file = "expt-start-time.txt"

# Multi-host parameters
num_hostpairs = 10
bottleneck_bandwidth_Mbps = 100
tcp_fraction = 1.00
total_cpu_fraction = 0.50
udp_server_throughput_suffix = "-udp.txt"
tcp_server_throughput_suffix = "-tcp.txt"
unthrottle_fraction = 0.00 # skip unthrottling for now
unthrottle_host_bandwidth_Mbps = 200
expt_start_suffix = "-start-time.txt"
epsilon = 11.0 # set to 1.0 for exact rate limiting, < 1 for host
              # bottleneck, > 1 for network bottleneck

class SetupTopo(Topo):
    """ Simple topology to setup 2 sources and bottleneck links to
    destination."""

    def __init__(self, **opts):
        """ Construct topology with num_hostpairs host pairs sending
        traffic through a single bottleneck link. """
        Topo.__init__(self, **opts)
        s1 = self.addSwitch('s1')
        s2 = self.addSwitch('s2')
        self.addLink(s1, s2) # bottleneck link
        hosts_src = []
        hosts_dst = []
        for i in range(0, num_hostpairs):
            hosts_src.append(self.addHost('hsrc' + str(i),
                                          cpu=total_cpu_fraction/(2*num_hostpairs)))
            hosts_dst.append(self.addHost('hdst' + str(i),
                                          cpu=total_cpu_fraction/(2*num_hostpairs)))
            self.addLink(hosts_src[-1], s1)
            self.addLink(hosts_dst[-1], s2)

def getHosts(net):
    """ Get host Node objects corresponding to host names """
    hosts_src = []
    hosts_dst = []
    for i in range(0, num_hostpairs):
        hosts_src.append(net.getNodeByName('hsrc' + str(i)))
        hosts_dst.append(net.getNodeByName('hdst' + str(i)))
    return [hosts_src, hosts_dst]

def getSwitches(net):
    """ Get switch Node objects corresponding to switch names """
    s1 = net.getNodeByName('s1')
    s2 = net.getNodeByName('s2')
    return [s1, s2]
    
def setupTc(net, hosts_src, switches_list):
    """ Set up rate limits and weighted queueing for the network """
    s1, s2 = switches_list

    for h in hosts_src:
        h.cmd("tc qdisc add dev " + h.name + "-eth0 root handle 1: "
              "htb default 1")
        h.cmd("tc class add dev " + h.name + "-eth0 parent 1: classid "
              "1:1 htb rate " +
              str(bottleneck_bandwidth_Mbps/num_hostpairs * epsilon) 
              + "Mbit")
        
    # s1-s2 link capacity
    s1.cmd("tc qdisc add dev s1-eth1 root handle 1: htb default 10")
    s1.cmd("tc class add dev s1-eth1 parent 1: classid 1:1 htb rate "
           + str(bottleneck_bandwidth_Mbps) + "Mbit")
    
    # s1-s2 weighted round robin queues
    s1.cmd("tc class add dev s1-eth1 parent 1:1 classid 1:10 htb rate "
           + str(tcp_fraction * bottleneck_bandwidth_Mbps)
           + "Mbit ceil " + str(bottleneck_bandwidth_Mbps) + "Mbit")

    s1.cmd("tc class add dev s1-eth1 parent 1:1 classid 1:11 htb rate "
           + str((1-tcp_fraction) * bottleneck_bandwidth_Mbps)
           + "Mbit ceil " + str(bottleneck_bandwidth_Mbps) + "Mbit")

    # s1-s2 queue classification based on protocol
    s1.cmd("tc filter add dev s1-eth1 protocol ip parent 1:0 prio 1"
           " u32 match ip protocol 6 0xff flowid 1:10")
    s1.cmd("tc filter add dev s1-eth1 protocol ip parent 1:0 prio 1"
           " u32  match ip protocol 17 0xff flowid 1:11")
 
    # s1-s2 shallow queue buffers -- 300 packets
    s1.cmd("tc qdisc add dev s1-eth1 parent 1:10 handle 20: pfifo"
           " limit 300")
    s1.cmd("tc qdisc add dev s1-eth1 parent 1:11 handle 30: pfifo"
           " limit 300")

def unthrottleFlow(hosts_src):
    """ Unthrottle some hosts such that s1--s2 now becomes the single
    unique bottleneck link."""
    print "Experiment time %d " % getExptTime()
    unthrottle_count = int(unthrottle_fraction * num_hostpairs)
    for i in range(0, unthrottle_count):
        h = hosts_src[i]
        cmd_str = ("tc class change dev " + h.name + "-eth0 classid "
                   "1:1 htb rate " +
                   str(unthrottle_host_bandwidth_Mbps) + "Mbit")
        print "Sending command", cmd_str, "to host", h.name
        h.cmd(cmd_str)

def startExpt():
    """ Record starting time of the experiment in the variable
    expt_start_time. To be used every time we want to know the time
    elapsed in the experiment. """
    global expt_start_time
    expt_start_time = time.time()
    f = open(start_time_file, "w")
    f.write(str(expt_start_time))
    f.close()

def getExptTime():
    """ Return time since the experiment started """
    return time.time() - expt_start_time
    
def runIperfTest(net, hosts_src, hosts_dst, switches_list):
    """ Run iperf testing between hosts in the network and record
    throughput results. """
    s1, s2 = switches_list

    # open files to note experiment timestamps
    expt_start_files = []
    for i in range(0, num_hostpairs):
        expt_start_files.append(open("hsrc" + str(i) + expt_start_suffix, 
                                     "w"))
    
    print "Starting iperf servers on flow destination hosts"
    tcp_hosts_count = int(tcp_fraction * num_hostpairs)
    for h in hosts_dst[0:tcp_hosts_count]: # TCP host pairs
        h.cmd("iperf -s -p 5003 -i 2 > " + h.name +
              tcp_server_throughput_suffix + " &")
    for h in hosts_dst[tcp_hosts_count:]:  # UDP host pairs
        h.cmd("iperf -u -s -p 5002 -i 2 >" + h.name +
              udp_server_throughput_suffix + "&")
    
    time.sleep(2) # this seems to be required to allow successful
                  # TCP connection establishment from clients

    print "Starting iperf client transmits"
    startExpt()
    
    udp_fraction = 1 - tcp_fraction
    udp_hosts_count = num_hostpairs - tcp_hosts_count
    for i in range(0, num_hostpairs):
        hsrc = hosts_src[i]
        hdst = hosts_dst[i]
        cmd_str = ''
        if i in range(0, tcp_hosts_count): # TCP host pair
            cmd_str = ("iperf -t " + str(test_duration_sec) + " -c " +
                       hdst.IP() + " -p 5003 &")
        else:
            cmd_str = ("iperf -t " + str(test_duration_sec) + " -c " + 
                       hdst.IP() + " -u -p 5002 -b " 
                       + str(udp_fraction * epsilon * 
                             bottleneck_bandwidth_Mbps * 
                             1000000 / udp_hosts_count)
                       + " &")
        print "Sending command", cmd_str, "to source", i
        hsrc.cmd(cmd_str)
        expt_start_files[i].write(str(getExptTime()))
        expt_start_files[i].close()

    # also setup unthrottling in a bit
    t = Timer(unthrottle_duration_sec, unthrottleFlow, [hosts_src])
    t.start()
    
def getTcStatCommand(protocol):
    """ Get tc stat command to send to host based on protocol
    (tcp/udp) or stat (drops/overlimits). """

    cmd = "tc -s -d class show dev s1-eth1 classid "

    if protocol == "tcp":
        cmd += "1:10"
    elif protocol == "udp":
        cmd += "1:11"
    elif protocol == "all":
        cmd = "tc -s -d qdisc show dev s1-eth1 "

    cmd += " | grep dropped"
    
    if protocol == "all":
        cmd += " | head -1"

    return cmd
    
def sendCommandWithTime(host, cmd):
    """ Return results from sending a command to a host """
    print "*** running command", cmd, "on host", host.name
    host.sendCmd(cmd)
    result = host.waitOutput()
    return str(getExptTime()) + " " + result.strip() + "\n"

def getTcStats(net, switches_list):
    """ Collect tc stats for drops and overlimits """
    s1, s2 = switches_list
    tcp_stats = open(tcp_stats_file, "w")
    udp_stats = open(udp_stats_file, "w")
    agg_stats = open(agg_stats_file, "w")

    end_time = time.time() + test_duration_sec
    while time.time() < end_time:
        # Record line containing sent, drops and overlimits
        # TCP stats
        cmd = getTcStatCommand("tcp")
        tcp_stats.write(sendCommandWithTime(s1, cmd))

        # UDP stats
        cmd = getTcStatCommand("udp")
        udp_stats.write(sendCommandWithTime(s1, cmd))

        # aggregate stats
        cmd = getTcStatCommand("all")
        agg_stats.write(sendCommandWithTime(s1, cmd))

        # sleep until next measurement, but also sample finer if
        # closer to unthrottle time.
        tsleep_sec = 2.0
        # if abs(getExptTime() - unthrottle_duration_sec) < 10:
        #     tsleep_sec = 2.0
        time.sleep(tsleep_sec)

    tcp_stats.close()
    udp_stats.close()
    agg_stats.close()

def pingFlowPairs(net, hosts_src, hosts_dst):
    """ Test connectivity between flow sources and destinations """
    assert len(hosts_src) == len(hosts_dst)
    for i in range(0, len(hosts_src)):
        result = hosts_src[i].cmd('ping -c1 %s' % (hosts_dst[i].IP()))
        sent, received = net._parsePing(result)
        print ('%d ' % i) if received else 'X '
        
def sharingTest():
    """ Main """
    topo = SetupTopo()
    # net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
    net = Mininet(topo=topo, host=CPULimitedHost)
    net.start()

    print "Dumping host connections"
    dumpNodeConnections(net.hosts)

    [hosts_src, hosts_dst] = getHosts(net)
    switches_list = getSwitches(net)

    print "Testing network connectivity"
    # net.pingAll()
    pingFlowPairs(net, hosts_src, hosts_dst)

    print "Setting up traffic control"
    setupTc(net, hosts_src, switches_list)

    # test workload in runIperfTest(...)
    print "Starting iperf tests"
    runIperfTest(net, hosts_src, hosts_dst, switches_list)
    
    print "Collecting tc stats... this may take a while (",
    print test_duration_sec, "seconds )"
    getTcStats(net, switches_list)

    CLI(net)
    
    net.stop()
    
if __name__ == '__main__':
    # Tell mininet to print useful information
    setLogLevel('info')
    sharingTest()

