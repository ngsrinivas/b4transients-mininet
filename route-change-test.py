#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.cli import CLI

import time
from threading import Timer

"""
  How to check if route changing actually works? Follow these sets of
  commands after dropping to CLI.

  h2 ip route add h3 dev h2-eth1 # change route on h2 to always use s3
     	      	     	 	 # as first hop

  # run tcpdump on interfaces on s1, s3, and may be s2 to check -- all
  # on separate terminals
  sudo tcpdump -i s1-eth3
  sudo tcpdump -i s3-eth3
  sudo tcpdump -i s2-eth2

  h2 ping -c 40 h3 # now ICMP echos start appearing in s3 tcpdump but
                   # not in s1
  
  # something like 10 seconds afterward, do
  h2 ip route change h3 dev h2-eth0	
  
  Now one should see ICMP echos and responses stop appearing in the s3
  tcpdump and start appearing in the s1 tcpdump.
"""

class SetupTopo(Topo):
    """ Simple topology to setup 2 sources and bottleneck links to
    destination."""

    def __init__(self, **opts):
        Topo.__init__(self, **opts)
        s1 = self.addSwitch('s1')
        s2 = self.addSwitch('s2')
        s3 = self.addSwitch('s3')
        h1 = self.addHost('h1', cpu=0.10)
        h2 = self.addHost('h2', cpu=0.10)
        h3 = self.addHost('h3', cpu=0.20)
        
        self.addLink(h1, s1)
        self.addLink(h2, s1)
        self.addLink(s1, s2)
        self.addLink(s2, h3)

        self.addLink(h1, s3)
        self.addLink(h2, s3)
        self.addLink(s3, s2)

def getHosts(net):
    """ Get host Node objects corresponding to host names """
    h1 = net.getNodeByName('h1')
    h2 = net.getNodeByName('h2')
    h3 = net.getNodeByName('h3')
    return [h1, h2, h3]

def getSwitches(net):
    """ Get switch Node objects corresponding to switch names """
    s1 = net.getNodeByName('s1')
    s2 = net.getNodeByName('s2')
    s3 = net.getNodeByName('s3')
    return [s1, s2, s3]
    
def routeTest():
    """ Main """
    topo = SetupTopo()
    # net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
    net = Mininet(topo=topo, host=CPULimitedHost)
    net.start()

    print "Dumping host connections"
    dumpNodeConnections(net.hosts)

    hosts_list = getHosts(net)
    switches_list = getSwitches(net)

    CLI(net)
    
    net.stop()
    
if __name__ == '__main__':
    # Tell mininet to print useful information
    setLogLevel('info')
    routeTest()

