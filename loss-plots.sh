#!/bin/bash

datapath="09-revised-assumptions/"
plotpath="09-revised-assumptions/figs"
plotscript="./pubplot.py"

for index in `echo 01 02`; do
    python $plotscript -a "font \"Helvetica,24\"" \
	-p 1 -f "postscript enhanced color" \
	-o ${plotpath}/$index-drop-rates-1s.eps \
	-x "Time (s)" -y "Loss rate (%)" \
	-k "top right vertical width 10 spacing 1.5 font \"Helvetica,24\"" \
	--bmargin 5 --lw 2 \
	--vl 60,180,300,420,540,660,780,900 \
	$datapath/$index/tcp-drop-rates.txt "tcp" "1:2" \
	$datapath/$index/udp-drop-rates.txt "udp" "1:2" \
	| gnuplot  ; epstopdf $plotpath/$index-drop-rates-1s.eps \
	--autorotate=All 

    python $plotscript -a "font \"Helvetica,24\"" \
	-p 1 -f "postscript enhanced color" \
	-o ${plotpath}/$index-queue-drain-1s.eps \
	-x "Time (s)" -y "Queue drain rate (Kbit/s)" \
	-k "top right vertical width 10 spacing 1.5 font \"Helvetica,24\"" \
	--bmargin 5 --lw 2 \
	--vl 60,180,300,420,540,660,780,900 \
	$datapath/$index/tcp-drop-rates.txt "tcp" "1:(\$4/100)" \
	$datapath/$index/udp-drop-rates.txt "udp" "1:(\$4/100)" \
	| gnuplot  ; epstopdf $plotpath/$index-queue-drain-1s.eps \
	--autorotate=All 

    python $plotscript -a "font \"Helvetica,24\"" \
	-p 1 -f "postscript enhanced color" \
	-o ${plotpath}/$index-drop-rates-25s.eps \
	-x "Time (s)" -y "Loss rate (%)" \
	-k "top right vertical width 10 spacing 1.5 font \"Helvetica,24\"" \
	--bmargin 5 --lw 3 \
	--vl 60,180,300,420,540,660,780,900 \
	$datapath/$index/tcp-drop-rates-25.txt "tcp" "1:2" \
	$datapath/$index/udp-drop-rates-25.txt "udp" "1:2" \
	| gnuplot  ; epstopdf $plotpath/$index-drop-rates-25s.eps \
	--autorotate=All 

    python $plotscript -a "font \"Helvetica,24\"" \
	-p 1 -f "postscript enhanced color" \
	-o ${plotpath}/$index-queue-drain-25s.eps \
	-x "Time (s)" -y "Queue drain rate (Kbit/s)" \
	-k "top right vertical width 10 spacing 1.5 font \"Helvetica,24\"" \
	--bmargin 5 --lw 3 \
	--vl 60,180,300,420,540,660,780,900 \
	$datapath/$index/tcp-drop-rates-25.txt "tcp" "1:(\$4/100)" \
	$datapath/$index/udp-drop-rates-25.txt "udp" "1:(\$4/100)" \
	| gnuplot  ; epstopdf $plotpath/$index-queue-drain-25s.eps \
	--autorotate=All 
done
