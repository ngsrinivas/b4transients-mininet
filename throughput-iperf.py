#!/usr/bin/python

"""
Construct the iperf time, throughput from TCP and UDP traces, and put
them into their "processed" files.
"""

import sys
import subprocess

# Globals definitions
# simple case: in the 3 host simple configuration
basic_tcp_iperf_input = "h3-tcp.txt"
basic_udp_iperf_input = "h3-udp.txt"
basic_tcp_iperf_output = "h3-tcp-processed.txt"
basic_udp_iperf_output = "h3-udp-processed.txt"

# multi-host configuration
tcp_server_throughput_suffix="-tcp.txt"
udp_server_throughput_suffix="-udp.txt"
tcp_fraction = 0.20
num_hostpairs = 50

def extract_tput(file_input, file_output):
    """ construct commands and extract output from data processing """
    tput_cmd = ("tail --lines=+7 " + file_input + " | awk "
                "-F- '{print $2}' | awk '$6 == \"Kbits/sec\" {print $1"
                "\" \" $5} $6 == \"Mbits/sec\" {print $1 \" \""
                "$5*1000} $6 == \"bits/sec\" {print $1 \" \" "
                "$5/1000.0}' > " + file_output)

    # diagnostic
    print tput_cmd

    # run commands in shell
    out = subprocess.check_output(tput_cmd, shell=True)

def processHost(src_dst_prefix, hostpair_index, tcp_hosts_count):
    """ Process a single host -- source or destination """
    input_file = src_dst_prefix + str(hostpair_index)
    if hostpair_index in range(0, tcp_hosts_count):
        input_file += tcp_server_throughput_suffix
    else:
        input_file += udp_server_throughput_suffix
    output_file = src_dst_prefix + str(hostpair_index) + "-processed.txt"
    extract_tput(input_file, output_file)
    
def main(argv=None):
    """ Main """
    global num_hostpairs
    if argv is None:
        argv = sys.argv
        
    fileset_pointer = 1
    if len(argv) > 1:
        fileset_pointer = int(argv[1])

    if fileset_pointer == 0:
        extract_tput(basic_tcp_iperf_input, basic_tcp_iperf_output)
        extract_tput(basic_udp_iperf_input, basic_udp_iperf_output)
    else:
        tcp_hosts_count = int(tcp_fraction * num_hostpairs)
        for i in range(0, num_hostpairs):
            processHost("hdst", i, tcp_hosts_count)
            processHost("hsrc", i, tcp_hosts_count)
        
if __name__ == "__main__":
    main()

