#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.cli import CLI

import time
from threading import Timer

# Global constants
# Various time values for tests
test_duration_sec = 600 # how long to run iperf tests?
unthrottle_duration_sec = 50 # initial time for unthrottle
expt_start_time = 0
# Files
tcp_stats_file = "tcp-drops-overlimits.txt"
udp_stats_file = "udp-drops-overlimits.txt"
agg_stats_file = "agg-drops-overlimits.txt"
start_time_file = "expt-start-time.txt"
# Topology parameters
num_hostpairs = 50
bottleneck_bandwidth_Mbps = 1
tcp_fraction = 0.80 # fraction of hosts/flows sending TCP
tcp_bottleneck_fraction = 0.80 # fraction of bottleneck link bandwidth
                               # in weighted round robin queue
total_cpu_fraction = 0.50
udp_server_throughput_suffix = "-udp.txt"
tcp_server_throughput_suffix = "-tcp.txt"
expt_start_suffix = "-start-time.txt"
epsilon = 1.1 # set to 1.0 for exact rate limiting, > 1 for network
              # bottleneck, < 1 for host bottleneck
enforce_cpu_limits = False
delays_enabled = True
round_trip_delay_ms = 50
bottleneck_buffer_pkts = 100

# stagger cycle related parameters
unthrottle_fraction = 1.28 # assert unthrottle_fraction >= epsilon
overthrottle_fraction = 0.92 # assert overthrottle_fraction <= epsilon
stagger_cycle_period = 90
transient_period = 10
new_steady_state_period = 50
stagger_affect_hosts = 10
total_stagger_hosts = int(tcp_fraction * num_hostpairs)

class SetupTopo(Topo):
    """ Simple topology to setup 2 sources and bottleneck links to
    destination."""

    def __init__(self, **opts):
        """ Construct topology with num_hostpairs host pairs sending
        traffic through a single bottleneck link. """
        Topo.__init__(self, **opts)
        s1 = self.addSwitch('s1')
        s2 = self.addSwitch('s2')
        self.addLink(s1, s2) # bottleneck link
        hosts_src = []
        hosts_dst = []
        for i in range(0, num_hostpairs):
            if enforce_cpu_limits:
                hosts_src.append(self.addHost('hsrc' + str(i),
                                              cpu=total_cpu_fraction/(2*num_hostpairs)))
                hosts_dst.append(self.addHost('hdst' + str(i),
                                              cpu=total_cpu_fraction/(2*num_hostpairs)))
            else:
                hosts_src.append(self.addHost('hsrc' + str(i)))
                hosts_dst.append(self.addHost('hdst' + str(i)))
            self.addLink(hosts_src[-1], s1)
            self.addLink(hosts_dst[-1], s2)

def getHosts(net):
    """ Get host Node objects corresponding to host names """
    hosts_src = []
    hosts_dst = []
    for i in range(0, num_hostpairs):
        hosts_src.append(net.getNodeByName('hsrc' + str(i)))
        hosts_dst.append(net.getNodeByName('hdst' + str(i)))
    return [hosts_src, hosts_dst]

def getSwitches(net):
    """ Get switch Node objects corresponding to switch names """
    s1 = net.getNodeByName('s1')
    s2 = net.getNodeByName('s2')
    return [s1, s2]
    
def setupTc(net, hosts_src, switches_list):
    """ Set up rate limits and weighted queueing for the network """
    s1, s2 = switches_list

    for h in hosts_src:
        h.cmd("tc qdisc add dev " + h.name + "-eth0 root handle 1: "
              "htb default 1")
        # HTB for bandwidth rate limiting
        h.cmd("tc class add dev " + h.name + "-eth0 parent 1: classid "
              "1:1 htb rate " +
              str(bottleneck_bandwidth_Mbps * epsilon/num_hostpairs) 
              + "Mbit")
        # specify link delays and host-side buffering
        if delays_enabled:
            h.cmd("tc qdisc add dev " + h.name + "-eth0 handle 10: "
                  "parent 1:1 netem %s limit 1000" % (
                    ("delay %dms" % round_trip_delay_ms) if
                    delays_enabled else "" ))
        
    # s1-s2 link capacity
    s1.cmd("tc qdisc add dev s1-eth1 root handle 1: htb default 10")
    s1.cmd("tc class add dev s1-eth1 parent 1: classid 1:1 htb rate "
           + str(bottleneck_bandwidth_Mbps) + "Mbit")
    
    # s1-s2 weighted round robin queues
    s1.cmd("tc class add dev s1-eth1 parent 1:1 classid 1:10 htb rate "
           + str(tcp_bottleneck_fraction * bottleneck_bandwidth_Mbps)
           + "Mbit ceil " + str(bottleneck_bandwidth_Mbps) + "Mbit")

    s1.cmd("tc class add dev s1-eth1 parent 1:1 classid 1:11 htb rate "
           + str((1-tcp_bottleneck_fraction) *
                 bottleneck_bandwidth_Mbps)
           + "Mbit ceil " + str(bottleneck_bandwidth_Mbps) + "Mbit")

    # s1-s2 queue classification based on protocol
    s1.cmd("tc filter add dev s1-eth1 protocol ip parent 1:0 prio 1"
           " u32 match ip protocol 6 0xff flowid 1:10")
    s1.cmd("tc filter add dev s1-eth1 protocol ip parent 1:0 prio 1"
           " u32  match ip protocol 17 0xff flowid 1:11")
 
    # s1-s2 shallow queue buffers -- 300 packets
    s1.cmd("tc qdisc add dev s1-eth1 parent 1:10 handle 20: pfifo"
           " limit " + str(bottleneck_buffer_pkts))
    s1.cmd("tc qdisc add dev s1-eth1 parent 1:11 handle 30: pfifo"
           " limit " + str(bottleneck_buffer_pkts))

def setThrottleLimits(hosts_src, host_counter, num_affected_hosts,
                      new_rl_Mbps):
    """Handle rate limit changes for specified hosts. """
    print "Experiment time %d " % getExptTime()
    for i in range(host_counter, host_counter + num_affected_hosts):
        h = hosts_src[i % total_stagger_hosts]
        cmd_str = ("tc class change dev " + h.name + "-eth0 parent "
                   "1: classid 1:1 htb rate " + str(new_rl_Mbps) +
                   "Mbit")
        print "Sending command", cmd_str, "to host", h.name
        h.cmd(cmd_str)

def staggerAssertionChecks():
    """Check some invariants for staggerThrottle """
    assert total_stagger_hosts >= 2 * 2 * stagger_affect_hosts
    assert stagger_cycle_period > (transient_period +
                                   new_steady_state_period)
    assert unthrottle_fraction >= epsilon
    assert overthrottle_fraction <= epsilon
    
def staggerThrottle(hosts_src, stage):
    """Implement staggered throttling of hosts such that:
    stage 1: stagger_affect_hosts hosts get unthrottled (network link
    becomes bottleneck)
    stage 2: stagger_affect_hosts different hosts get overthrottled
    (hosts become bottleneck)
    stage 3: restore default limits for all hosts
    
    Note that number of hosts should be >= 2 * 2 *
    stagger_affect_hosts, so that there are at least 2 full cycles
    (stages 1--3) without overlapping hosts.
    
    The time period through all 3 cycles is stagger_cycle_period,
    between stages 1--2 is transient_period, and stages 2--3 is
    new_steady_state_period. The invariant stagger_cycle_period >
    transient_period + new_steady_state_period should be maintained
    at all times.
    """
    # setup static variable to remember position in the stagger cycle
    # through all host pairs.
    if "stagger_host_counter" not in staggerThrottle.__dict__:
        staggerThrottle.stagger_host_counter = 0

    # only continue if there is experiment time remaining
    if getExptTime() > test_duration_sec:
        return 0

    # state machine for staggered throttles and unthrottles
    if stage == 1:
        host_ctr = staggerThrottle.stagger_host_counter
        target_bandwidth = (bottleneck_bandwidth_Mbps *
                            unthrottle_fraction / num_hostpairs)
        setThrottleLimits(hosts_src, host_ctr, stagger_affect_hosts,
                          target_bandwidth)
        t = Timer(transient_period, staggerThrottle, [hosts_src, 2])
        t.start()
    elif stage == 2:
        host_ctr = (staggerThrottle.stagger_host_counter +
                    stagger_affect_hosts)
        target_bandwidth = (bottleneck_bandwidth_Mbps *
                            overthrottle_fraction / num_hostpairs)
        setThrottleLimits(hosts_src, host_ctr, stagger_affect_hosts,
                          target_bandwidth)
        t = Timer(new_steady_state_period, staggerThrottle,
                  [hosts_src, 3])
        t.start()
    elif stage == 3:
        remaining_period = stagger_cycle_period - (transient_period +
                                                   new_steady_state_period)
        host_ctr = staggerThrottle.stagger_host_counter
        target_bandwidth = (bottleneck_bandwidth_Mbps * epsilon /
                            num_hostpairs)
        setThrottleLimits(hosts_src, host_ctr, 2*stagger_affect_hosts,
                          target_bandwidth)
        staggerThrottle.stagger_host_counter = ((
                staggerThrottle.stagger_host_counter + 2 *
                stagger_affect_hosts) % total_stagger_hosts)
        t = Timer(remaining_period, staggerThrottle, [hosts_src, 1])
        t.start()

def startExpt():
    """ Record starting time of the experiment in the variable
    expt_start_time. To be used every time we want to know the time
    elapsed in the experiment. """
    global expt_start_time
    expt_start_time = time.time()
    f = open(start_time_file, "w")
    f.write(str(expt_start_time))
    f.close()

def getExptTime():
    """ Return time since the experiment started """
    return time.time() - expt_start_time
    
def runIperfTest(net, hosts_src, hosts_dst, switches_list):
    """ Run iperf testing between hosts in the network and record
    throughput results. """
    s1, s2 = switches_list

    # open files to note experiment timestamps
    expt_start_files = []
    for i in range(0, num_hostpairs):
        expt_start_files.append(open("hsrc" + str(i) + expt_start_suffix, 
                                     "w"))
    
    print "Starting iperf servers on flow destination hosts"
    tcp_hosts_count = int(tcp_fraction * num_hostpairs)
    for h in hosts_dst[0:tcp_hosts_count]: # TCP host pairs
        h.cmd("iperf -s -p 5003 -i 1 > " + h.name +
              tcp_server_throughput_suffix + " &")
    for h in hosts_dst[tcp_hosts_count:]:  # UDP host pairs
        h.cmd("iperf -u -s -p 5002 -i 1 >" + h.name +
              udp_server_throughput_suffix + " &")
    
    time.sleep(2) # this seems to be required to allow successful
                  # TCP connection establishment from clients

    print "Starting iperf client transmits"
    startExpt()
    
    udp_fraction = 1 - tcp_fraction
    udp_hosts_count = num_hostpairs - tcp_hosts_count
    for i in range(0, num_hostpairs):
        hsrc = hosts_src[i]
        hdst = hosts_dst[i]
        cmd_str = ''
        if i in range(0, tcp_hosts_count): # TCP host pair
            cmd_str = ("iperf -t " + str(test_duration_sec) + " -c " +
                       hdst.IP() + " -p 5003 -i 1 > " + hsrc.name +
                       tcp_server_throughput_suffix + " &")
        else:
            cmd_str = ("iperf -t " + str(test_duration_sec) + " -c " + 
                       hdst.IP() + " -u -p 5002 -i 1 -b " 
                       + str(udp_fraction * epsilon * 
                             bottleneck_bandwidth_Mbps * 
                             1000000 / udp_hosts_count)
                       + " > " + hsrc.name + udp_server_throughput_suffix
                       + " &")
        print "Sending command", cmd_str, "to source", i
        hsrc.cmd(cmd_str)
        expt_start_files[i].write(str(getExptTime()))
        expt_start_files[i].close()

    # also setup unthrottling in a bit
    t = Timer(unthrottle_duration_sec, staggerThrottle, [hosts_src, 1])
    t.start()
    
def getTcStatCommand(protocol):
    """ Get tc stat command to send to host based on protocol
    (tcp/udp) or stat (drops/overlimits). """

    cmd = "tc -s -d class show dev s1-eth1 classid "

    if protocol == "tcp":
        cmd += "1:10"
    elif protocol == "udp":
        cmd += "1:11"
    elif protocol == "all":
        cmd = "tc -s -d qdisc show dev s1-eth1 "

    cmd += " | grep dropped"
    
    if protocol == "all":
        cmd += " | head -1"

    return cmd
    
def sendCommandWithTime(host, cmd):
    """ Return results from sending a command to a host """
    print "*** running command", cmd, "on host", host.name
    host.sendCmd(cmd)
    result = host.waitOutput()
    return str(getExptTime()) + " " + result.strip() + "\n"

def getTcStats(net, switches_list):
    """ Collect tc stats for drops and overlimits """
    s1, s2 = switches_list
    tcp_stats = open(tcp_stats_file, "w")
    udp_stats = open(udp_stats_file, "w")
    agg_stats = open(agg_stats_file, "w")

    end_time = time.time() + test_duration_sec
    while time.time() < end_time:
        # Record line containing sent, drops and overlimits
        # TCP stats
        cmd = getTcStatCommand("tcp")
        tcp_stats.write(sendCommandWithTime(s1, cmd))

        # UDP stats
        cmd = getTcStatCommand("udp")
        udp_stats.write(sendCommandWithTime(s1, cmd))

        # aggregate stats
        cmd = getTcStatCommand("all")
        agg_stats.write(sendCommandWithTime(s1, cmd))

        # sleep until next measurement
        tsleep_sec = 1.0
        time.sleep(tsleep_sec)

    tcp_stats.close()
    udp_stats.close()
    agg_stats.close()

def pingFlowPairs(net, hosts_src, hosts_dst):
    """ Test connectivity between flow sources and destinations """
    assert len(hosts_src) == len(hosts_dst)
    for i in range(0, len(hosts_src)):
        result = hosts_src[i].cmd('ping -c1 %s' % (hosts_dst[i].IP()))
        sent, received = net._parsePing(result)
        print ('%d ' % i) if received else 'X '
        
def sharingTest():
    """ Main """
    topo = SetupTopo()
    # net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
    net = Mininet(topo=topo, host=CPULimitedHost)
    net.start()

    print "Dumping host connections"
    dumpNodeConnections(net.hosts)

    [hosts_src, hosts_dst] = getHosts(net)
    switches_list = getSwitches(net)

    print "Testing network connectivity"
    # net.pingAll()
    pingFlowPairs(net, hosts_src, hosts_dst)

    print "Setting up traffic control"
    setupTc(net, hosts_src, switches_list)
    
    # check some basic conditions for staggering throttle runs
    staggerAssertionChecks()

    # test workload in runIperfTest(...)
    print "Starting iperf tests"
    runIperfTest(net, hosts_src, hosts_dst, switches_list)
    
    print "Collecting tc stats... this may take a while (",
    print test_duration_sec, "seconds )"
    getTcStats(net, switches_list)

    CLI(net)
    
    net.stop()
    
if __name__ == '__main__':
    # Tell mininet to print useful information
    setLogLevel('info')
    sharingTest()

