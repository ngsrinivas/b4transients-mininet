#!/usr/bin/python

"""
Calculate drop rates from drop counters, and write down loss rates in
number of packets dropped per packet that came in.

Supply an optional argument to indicate at what time granularity the
drop rates should be averaged.

"""

import sys

# globals definitions
tcp_stats_file = "tcp-drops-overlimits.txt"
udp_stats_file = "udp-drops-overlimits.txt"
agg_stats_file = "agg-drops-overlimits.txt"
tcp_drop_rates = "tcp-drop-rates.txt"
udp_drop_rates = "udp-drop-rates.txt"
agg_drop_rates = "agg-drop-rates.txt"

bottleneck_bandwidth_Mbps = 1

def main(argv=None):
    """ Main """
    global tcp_drop_rates, udp_drop_rates, agg_drop_rates
    
    if argv is None:
        argv = sys.argv

    averaging_period = -1 # indicates finest granularity available
                          # from data
    if len(argv) > 1:
        averaging_period = float(argv[1])

    if averaging_period > 0:
        tcp_drop_rates = "tcp-drop-rates-%d.txt" % averaging_period
        udp_drop_rates = "udp-drop-rates-%d.txt" % averaging_period
        agg_drop_rates = "agg-drop-rates-%d.txt" % averaging_period
        
    stats_files = [tcp_stats_file, udp_stats_file, agg_stats_file]
    drop_rates = [tcp_drop_rates, udp_drop_rates, agg_drop_rates]
        
    for index in range(0, 3):
        stats_file = open(stats_files[index], "r")
        rates_file = open(drop_rates[index], "w")

        prev_dropped = -1
        prev_sent_pkts = -1
        prev_sent_bytes = -1
        prev_overlimits = -1
        prev_sampling_time = -1
        first_line = True

        for line in stats_file:
            parts = line.strip().split(" ")
            sampling_time = float(parts[0])
            dropped = float(parts[7][:-1]) # [:-1] to remove trailing ','
            overlimits = float(parts[9])
            sent_pkts = float(parts[4])
            sent_bytes = float(parts[2])
            wrote_line = False
            
            if prev_sent_pkts != -1 and sent_pkts != prev_sent_pkts:
                if (averaging_period == -1 or 
                    ((averaging_period > 0.0 and 
                      (sampling_time - prev_sampling_time) >
                      averaging_period))):
                    drop_rate = ((dropped - prev_dropped) / 
                                 ((sent_pkts - prev_sent_pkts) + 
                                  (dropped - prev_dropped)) * 100)
                    overlimits_rate = ((overlimits - prev_overlimits)
                                       / (sent_pkts - prev_sent_pkts)
                                       * 100.0)
                    utilization = (((sent_bytes - prev_sent_bytes) *
                                    8.0 * 100.0) /
                                   (1000.0 * (sampling_time -
                                              prev_sampling_time) *
                                    bottleneck_bandwidth_Mbps))
                    rates_file.write(str(sampling_time) + " " +
                                     str(drop_rate) + " " +
                                     str(overlimits_rate) + " " +
                                     str(utilization) + "\n")
                    wrote_line = True

            if first_line or wrote_line:
                prev_dropped = dropped
                prev_sent_pkts = sent_pkts
                prev_sent_bytes = sent_bytes
                prev_overlimits = overlimits
                prev_sampling_time = sampling_time
                first_line = False

        stats_file.close()
        rates_file.close()

if __name__ == "__main__":
    main(sys.argv)

